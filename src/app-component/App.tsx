import * as React from "react";
import "./App.css";
import { Container, Col, Row } from "reactstrap";
import { ConnectedVehicleTable } from "../vehicle-table";
import { ConnectedDetailsPanel } from "../details-panel";

interface IAppProps {
  onLoad: () => void;
}

export class App extends React.Component<IAppProps> {
  public componentDidMount() {
    this.props.onLoad();
  }

  public render() {
    return (
      <Container>
        <Row>
          <Col xl="auto">
            <ConnectedVehicleTable />
          </Col>
          <Col>
            <ConnectedDetailsPanel />
          </Col>
        </Row>
      </Container>
    );
  }
}

