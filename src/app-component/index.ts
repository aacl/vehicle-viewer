import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { loadData } from './app-data-management';
import { App } from './App';

export { reducer } from './app-data-management';
export default connect(
    state => ({}),
    (dispatch: ThunkDispatch<{}, {}, any>) => {
      return {
        onLoad: () => {
          dispatch(loadData());
        }
      };
    }
  )(App);
