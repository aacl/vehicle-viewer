import {
  createAction,
  createSlice,
  PayloadAction,
  Dispatch,
  Action
} from "@reduxjs/toolkit";
import { VehicleDto, Vehicle } from "../api-types";

export const beginInitDataAction = createAction("app/begin-init-data");
export const finishInitData = createAction<VehicleDto>("app/finish-init-data");
export const errorInitData = createAction("app/error-in-init-data");

export interface VehicleStoreState {
  isLoading: boolean;
  data: Vehicle[];
}

const initialState: VehicleStoreState = { isLoading: false, data: [] };

const vehicles = createSlice({
  name: "vehicles",
  initialState,
  reducers: {
    initDataStart: (state): VehicleStoreState => {
      return { ...state, isLoading: true };
    },
    initDataSuccess: (state, action: PayloadAction<VehicleDto>) => {
      return {
        ...state,
        isLoading: false,
        data: action.payload.vehicles
      };
    },
    initDataFailure: state => {
      return {
        ...state,
        isLoading: false
      };
    }
  }
});

export const { ...x } = vehicles.actions;

export const reducer = vehicles.reducer;

export function loadData() {
  return async (dispatch: Dispatch<Action<any>>, getState: () => any) => {
    dispatch(vehicles.actions.initDataStart());
    try {
      const result = await fetch("https://api.myjson.com/bins/11melk", {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      });

      if (result.status >= 200 || result.status <= 302) {
        const json: VehicleDto = await result.json();
        return dispatch(vehicles.actions.initDataSuccess(json));
      } else {
        return dispatch(vehicles.actions.initDataFailure());
      }
    } catch (e) {
      return dispatch(vehicles.actions.initDataFailure());
    }
  };
}
