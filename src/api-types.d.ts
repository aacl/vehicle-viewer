export interface VehicleDto {
    vehicles: Vehicle[];
}

export interface Vehicle {
    name:             string;
    type:             string;
    created:          string;
    horsepower:       number;
    serviceEvents:    ServiceEvent[];
    odometerReadings: OdometerReading[];
}

export interface OdometerReading {
    date:  string;
    miles: number;
}

export interface ServiceEvent {
    eventId:           number;
    date:              string;
    servicesPerformed: ServicesPerformed[];
    itemsSold:         ItemsSold;
}

export interface ItemsSold {
    items:    Item[];
    freebies: Freeby[];
}

export interface Freeby {
    fType: FType;
    count: number;
}

export interface FType {
    name: string;
}

export interface Item {
    itemType: ItemType;
    units:    number;
}

export interface ItemType {
    name:         string;
    pricePerUnit: number;
}

export interface ServicesPerformed {
    serviceType: ServiceType;
    hours:       number;
}

export interface ServiceType {
    name:         string;
    pricePerHour: number;
}
