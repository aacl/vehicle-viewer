export interface IVehicleData {
  name: string;
  type: string;
  horsepower: number;
  created: string;
}

export type Direction = "asc" | "desc";
