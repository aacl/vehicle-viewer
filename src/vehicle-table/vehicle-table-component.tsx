import * as React from "react";
import { Table, Button } from "reactstrap";
import { Direction, IVehicleData } from "./types";
import { sortFunctionFactory } from "./sort-function-factory";

import { IoIosArrowUp, IoIosArrowDown } from "react-icons/io";

class VehicleTableColumnHeaders extends React.Component<{
  sort: (direction: Direction, field: string) => void;
}> {
  private sortAscending = (e: React.MouseEvent<SVGElement, MouseEvent>) => {
    const field = e.currentTarget.getAttribute("data-column-field");
    if (field) {
      this.props.sort("asc", field);
    }
  };

  private sortDescending = (e: React.MouseEvent<SVGElement, MouseEvent>) => {
    const field = e.currentTarget.getAttribute("data-column-field");
    if (field) {
      this.props.sort("desc", field);
    }
  };

  public render() {
    return (
      <thead>
        <tr>
          <th>
            Name
            <IoIosArrowUp
              data-column-field="name"
              onClick={this.sortAscending}
            />
            <IoIosArrowDown
              data-column-field="name"
              onClick={this.sortDescending}
            />
          </th>
          <th>
            Type
            <IoIosArrowUp
              data-column-field="type"
              onClick={this.sortAscending}
            />
            <IoIosArrowDown
              data-column-field="type"
              onClick={this.sortDescending}
            />
          </th>
          <th>
            Horse Power
            <IoIosArrowUp
              data-column-field="horse power"
              onClick={this.sortAscending}
            />
            <IoIosArrowDown
              data-column-field="horse power"
              onClick={this.sortDescending}
            />
          </th>
          <th>
            Created
            <IoIosArrowUp
              data-column-field="created"
              onClick={this.sortAscending}
            />
            <IoIosArrowDown
              data-column-field="created"
              onClick={this.sortDescending}
            />
          </th>
        </tr>
      </thead>
    );
  }
}

function VehicleTableRow(
  props: IVehicleData & {
    showDetails: (e: React.MouseEvent<any, MouseEvent>) => void;
  }
) {
  return (
    <tr>
      <td>{props.name}</td>
      <td>{props.type}</td>
      <td>{props.horsepower}</td>
      <td>{props.created}</td>
      <td>
        <Button
          data-name={props.name}
          data-created={props.created}
          color="primary"
          onClick={props.showDetails}
        >
          details
        </Button>
      </td>
    </tr>
  );
}

interface IVehicleTableProps {
  data: IVehicleData[];
  showDetails: (name: string, string: string) =>  void;
}

interface IVehicleTableState {
  sort?: (a: IVehicleData[]) => IVehicleData[];
}

export class VehicleTable extends React.Component<
  IVehicleTableProps,
  IVehicleTableState
> {
  constructor(props: IVehicleTableProps) {
    super(props);
    this.state = {};
  }

  private sortByColumn = (direction: Direction, field: string) => {
    const sortFunction = sortFunctionFactory(direction, field);
    this.setState(() => ({ sort: sortFunction }));
  };

  private showDetails = (e: React.MouseEvent<any, MouseEvent>) => {
    const el = e.currentTarget;
    const name = el.getAttribute('data-name');
    const date = el.getAttribute('data-created');
    if (name && date) {
      this.props.showDetails(name, date);
    }
  };

  public render() {
    const data = this.state.sort
      ? this.state.sort(this.props.data)
      : this.props.data;
    return (
      <Table>
        <VehicleTableColumnHeaders sort={this.sortByColumn} />
        <tbody>
          {data.map(x => (
            <VehicleTableRow key={x.name} {...x} showDetails={this.showDetails} />
          ))}
        </tbody>
      </Table>
    );
  }
}
