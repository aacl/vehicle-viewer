import { Direction, IVehicleData } from "./types";
import { sort } from "ramda";
import { parse } from "date-fns";

function reverseCompareForDescOrder(compareResult: number) {
  switch (compareResult) {
    case 1:
      return -1;
    case -1:
      return 1;
    default:
      return 0;
  }
}

function compareStrings(a: string, b: string): number {
  return a.localeCompare(b);
}

function compareNumber(a: number, b: number): number {
  if (a > b) return 1;
  else if (b > a) return -1;
  else return 0;
}

function formatForOrderBy(direction: Direction, result: number) {
  return direction === "desc" ? reverseCompareForDescOrder(result) : result;
}

function sortByName(
  direction: Direction
): (a: IVehicleData, b: IVehicleData) => number {
  return (a: IVehicleData, b: IVehicleData) => {
    const result = compareStrings(a.name, b.name);
    return formatForOrderBy(direction, result);
  };
}

function sortByType(
  direction: Direction
): (a: IVehicleData, b: IVehicleData) => number {
  return (a: IVehicleData, b: IVehicleData) => {
    const result = compareStrings(a.type, b.type);
    return formatForOrderBy(direction, result);
  };
}

function sortByHorsePower(
  direction: Direction
): (a: IVehicleData, b: IVehicleData) => number {
  return (a: IVehicleData, b: IVehicleData) => {
    const result = compareNumber(a.horsepower, b.horsepower);
    return formatForOrderBy(direction, result);
  };
}

function parseCreated(date: string) {
  return parse(date, "yyyy-MM-dd hh:mmaaaaa", new Date());
}

function sortByCreated(
  direction: Direction
): (a: IVehicleData, b: IVehicleData) => number {
  return (a: IVehicleData, b: IVehicleData) => {
    const result = compareNumber(
      parseCreated(a.created).valueOf(),
      parseCreated(b.created).valueOf()
    );
    return formatForOrderBy(direction, result);
  };
}

export function sortFunctionFactory(
  direction: Direction,
  field: string
): (data: IVehicleData[]) => IVehicleData[] {
  let compareFunc: (a: IVehicleData, b: IVehicleData) => number;
  switch (field.toLowerCase()) {
    case "name":
      compareFunc = sortByName(direction);
      break;
    case "type":
      compareFunc = sortByType(direction);
      break;
    case "horse power":
      compareFunc = sortByHorsePower(direction);
      break;
    case "created":
      compareFunc = sortByCreated(direction);
      break;
    default:
      compareFunc = sortByName(direction);
      break;
  }

  return (data: IVehicleData[]) => sort(compareFunc, data);
}
