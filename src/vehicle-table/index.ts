import { connect } from "react-redux";
import { VehicleTable } from "./vehicle-table-component";
import { selectVehicle } from "../details-panel";
import { IStore } from "../store";

function mapStoreToProps(store: IStore) {
  return {
    data: store.vehicle.data
  };
}
export const ConnectedVehicleTable = connect(mapStoreToProps, {
  showDetails: (name: string, date: string) => selectVehicle({ name, date })
})(VehicleTable);
