import { Collapse, CardBody, Card, ListGroupItem, ListGroup } from "reactstrap";
import * as React from "react";

export interface IDetailsPanelProps {
  isOpen: boolean;
  servicings: string[];
  odometerReadings: string[];
}

export function DetailsPanel(props: IDetailsPanelProps) {
  return (
    <Collapse isOpen={props.isOpen}>
      <Card>
        <CardBody>
          <h3>Odometer Readings</h3>
          <ListGroup>
            {props.odometerReadings.map((x, idx) => (
              <ListGroupItem key={idx}>{x}</ListGroupItem>
            ))}
          </ListGroup>
          <br />
          <h3>Services</h3>
          <ListGroup>
            {props.servicings.map((x, idx) => (
              <ListGroupItem key={idx}>{x}</ListGroupItem>
            ))}
          </ListGroup>
        </CardBody>
      </Card>
    </Collapse>
  );
}
