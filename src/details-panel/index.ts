import { connect } from "react-redux";
import { DetailsPanel, IDetailsPanelProps } from "./details-panel-component";
import { IStore } from "../store";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { OdometerReading, ServiceEvent } from "../api-types";

export interface ISelectedVehicle {
  name?: string;
  date?: string;
  isPopulated: boolean;
}

const initialState: ISelectedVehicle = { isPopulated: false };

const selectedVehicle = createSlice({
  name: "selectedVehicle",
  initialState,
  reducers: {
    selectVehicle: (
      state,
      action: PayloadAction<{ name: string; date: string }>
    ): ISelectedVehicle => ({
      ...state,
      isPopulated: true,
      name: action.payload.name,
      date: action.payload.date
    })
  }
});

export const { reducer } = selectedVehicle;
export const { selectVehicle } = selectedVehicle.actions;

function createOdomoterString(reading: OdometerReading) {
    return `${reading.miles} miles on ${reading.date}`;
}

function createServiceString(service: ServiceEvent) {
    const services = service.servicesPerformed.map(x => `\t${x.serviceType.name}`)
    return `Service on ${service.date}:\n${services}`;
}

function mapStateToProps(store: IStore): IDetailsPanelProps {
  if (store.selectedVehicle.isPopulated) {
    const vehicle = store.vehicle.data.filter(
      x =>
        store.selectedVehicle.date! === x.created &&
        store.selectedVehicle.name! === x.name
    );

    if (vehicle.length === 1) {
        return {
          isOpen: true,
          odometerReadings: vehicle[0].odometerReadings.map(createOdomoterString),
          servicings: vehicle[0].serviceEvents.map(createServiceString)
        };
    }
  }

  return {
    isOpen: false,
    odometerReadings: [],
    servicings: []
  };
}

export const ConnectedDetailsPanel = connect(mapStateToProps)(DetailsPanel);
