import { configureStore, combineReducers, getDefaultMiddleware } from '@reduxjs/toolkit';
import { reducer as vehicleReducer } from './app-component';
import { reducer as selectedVehicle, ISelectedVehicle } from './details-panel';
import { VehicleStoreState } from './app-component/app-data-management';

export interface IStore {
    vehicle: VehicleStoreState
    selectedVehicle: ISelectedVehicle;
}

const reducer = combineReducers({
    vehicle: vehicleReducer,
    selectedVehicle
})
export const store = configureStore({
    reducer,
    middleware: [...getDefaultMiddleware(),]
});
